import 'package:flutter/material.dart';
import 'package:zefyrka/zefyrka.dart';

class WriteScreen extends StatelessWidget {
  String result = '';
  TextEditingController _nameController = TextEditingController();
  TextEditingController _titleController = TextEditingController();

  ZefyrController _controller = ZefyrController();

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: [
          Container(
            height: 70,
            child: Row(
              children: [
                Expanded(
                  child: Container
                    (
                    width: 100,
                    color: Colors.deepPurple,
                    child: Row(
                      children: [
                        Container(
                          width: 150,
                          color: Colors.red,
                          child: ElevatedButton(
                            onPressed: () { Navigator.pop(context); },
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(Colors.red)
                            ),
                            child: Text(
                              "뒤로가기",
                              style: TextStyle(
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                ),
                Expanded(
                  child: ElevatedButton(
                    onPressed: () {
                      var name = _nameController.value.text;
                      var title = _titleController.value.text;
                      var content = _controller.plainTextEditingValue.text;
                      print(name);
                      print(title);
                      print(content);

                    },
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.red)
                    ),
                    child: Text(
                      "제출 버튼",
                      style: TextStyle(
                        color: Colors.lightBlueAccent,
                      ),
                    ),
                  ),
                ),
                Expanded(child: Container())
              ],
            ),
          ),
          Container(
            width: 1000,
            height: 30,
            child: TextField(
              controller: _nameController,
              decoration: InputDecoration.collapsed(
                  hintText: 'Username'
              ),
            ),
          ),
          Container(
              width: 1000,
              child: Divider()),
          Container(
            width: 1000,
            height: 30,
            child: TextField(
              controller: _titleController,
              decoration: InputDecoration.collapsed(
                  hintText: 'Title'
              ),
            ),
          ),
          ZefyrToolbar.basic(controller: _controller),
          ZefyrTheme(data: ZefyrThemeData(
            link: TextStyle(
                color: Colors.blue
            ),
            heading2: TextBlockTheme(
                spacing: VerticalSpacing(),
                style: TextStyle(
                    color: Colors.blue
                )
            ),
            quote: TextBlockTheme(
                spacing: VerticalSpacing(),
                style: TextStyle(
                    color: Colors.blue
                )
            ),
            underline: TextStyle(
                color: Colors.blue
            ),
            lists: TextBlockTheme(
                spacing: VerticalSpacing(),
                style: TextStyle(
                    color: Colors.blue
                )
            ),
            italic: TextStyle(
                color: Colors.blue
            ),
            paragraph: TextBlockTheme(
                spacing: VerticalSpacing(),
                style: TextStyle(
                    color: Colors.blue
                )
            ),
            heading3: TextBlockTheme(
                spacing: VerticalSpacing(),
                style: TextStyle(
                    color: Colors.blue
                )
            ),
            strikethrough: TextStyle(
                color: Colors.blue
            ),
            bold: TextStyle(
                color: Colors.blue
            ),
            heading1: TextBlockTheme(
                spacing: VerticalSpacing(),
                style: TextStyle(
                    color: Colors.blue
                )
            ),
            code: TextBlockTheme(
                spacing: VerticalSpacing(),
                style: TextStyle(
                    color: Colors.blue
                )
            ),
          ), child: Container(
            width: 1000,
            height: 1000,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.blueAccent)
            ),
            child: ZefyrEditor(

              controller: _controller,
            ),
          ),),

        ],
      ),
    );
  }
}

