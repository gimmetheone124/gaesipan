import 'package:dcinside_gaesipal/writeScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {

      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GaeSi(),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}


class GaeSi extends StatefulWidget {
  const GaeSi({Key? key}) : super(key: key);
//original data

  @override
  _GaeSiState createState() => _GaeSiState();
}

class _GaeSiState extends State<GaeSi> {
  List<String> entries = <String>['A','B','C'];
  List<int> colorCodes = <int>[600, 500, 400];
  var data = {
    {
      "번호" : 2327,
      "말머리" : "설문",
      "제목" : "손절없이 오랫동안 우정 지킬 것 같은 스타는?",
      "글쓴이" : "운영자",
      "작성일" : "22/08/15",
      "조회" : "-",
      "추천" : "-",
    },
    {
      "번호" : 4317,
      "말머리" : "공지",
      "제목" : "개정 후 정보처리기사/산업기사 합격률 정리 [14]",
      "글쓴이" : "hyTnic",
      "작성일" : "21.08.19",
      "조회" : 7099,
      "추천" : 21,
    },
    {
      "번호" : 18329,
      "말머리" : "공지",
      "제목" : "2023~2025년 정보처리기사 출제기준 발표 [19]",
      "글쓴이" : "hyTnic",
      "작성일" : "01:28",
      "조회" : 2460,
      "추천" : 16,
    },
    {
      "번호" : 9119,
      "말머리" : "공지",
      "제목" : "2021년 정처기갤내 합격자 공부자료 조사결과정리 [2]",
      "글쓴이" : "hyTnic",
      "작성일" : "01:28",
      "조회" : 4185,
      "추천" : 14,
    },
    {
      "번호" : 8895,
      "말머리" : "공지",
      "제목" : "2022년 정보처리기사 시험일정 [6]",
      "글쓴이" : "hyTnic",
      "작성일" : "21.12.01",
      "조회" : 3625,
      "추천" : 17,
    },
    {
      "번호" : 2909,
      "말머리" : "공지",
      "제목" : "차단 및 글삭기준 [4]",
      "글쓴이" : "hyTnic",
      "작성일" : "21.04.26",
      "조회" : 1783,
      "추천" : 15,
    },
    {
      "번호" : 22301,
      "말머리" : "일반",
      "제목" : "님들은 기능사 시험 왜 보셨나요 [1]",
      "글쓴이" : "ㅇㅇ",
      "작성일" : "01:28",
      "조회" : 48,
      "추천" : 0,
    },
    {
      "번호" : 22300,
      "말머리" : "일반",
      "제목" : "큐넷에서 기사 응시조건 보고 헷갈려서 지부에 전화했는데 대답이 아리까리하 [3]",
      "글쓴이" : "질문드립니다(121.149)",
      "작성일" : "00:15",
      "조회" : 70,
      "추천" : 1,
    },
    {
      "번호" : 22299,
      "말머리" : "일반",
      "제목" : "정처기 59받고나서 5번 떨어졌는데 저주받았나봐 [1]",
      "글쓴이" : "ㅇㅇ",
      "작성일" : "08.15",
      "조회" : 75,
      "추천" : 0,
    },
    {
      "번호" : 22298,
      "말머리" : "일반",
      "제목" : "정처산기 실기도 html.css.자바스크립트 나옴?	ㅇㅇ(121.163)",
      "글쓴이" : "ㅇㅇ(121.163)",
      "작성일" : "08.15",
      "조회" : 26,
      "추천" : 0,
    },
    {
      "번호" : 22296,
      "말머리" : "일반",
      "제목" : "3회실기 준비하는놈 있냐? [5]",
      "글쓴이" : "ㅇㅇ(39.7)",
      "작성일" : "08.15",
      "조회" : 165,
      "추천" : 1,
    },
    {
      "번호" : 22295,
      "말머리" : "일반",
      "제목" : "님들은 기능사 시험 왜 보셨나요",
      "글쓴이" : "ㅇㅇ(39.7)",
      "작성일" : "08.15",
      "조회" : 120,
      "추천" : 0,
    },
    {
      "번호" : 22294,
      "말머리" : "일반",
      "제목" : "컴활1급>정처기",
      "글쓴이" : "ㅇㅇ",
      "작성일" : "08.15",
      "조회" : 268,
      "추천" : 0,
    },
    {
      "번호" : 22292,
      "말머리" : "일반",
      "제목" : "기능사 2번 문제 [3]",
      "글쓴이" : "",
      "작성일" : "08.15",
      "조회" : 131,
      "추천" : 0,
    },
    {
      "번호" : 22290,
      "말머리" : "일반",
      "제목" : "실기공부 진짜 애매하네;;; [2]",
      "글쓴이" : "",
      "작성일" : "08.15",
      "조회" : 172,
      "추천" : 0,
    },
    {
      "번호" : 22289,
      "말머리" : "일반",
      "제목" : "정보처리 찍기능사 ㅋㅋ",
      "글쓴이" : "ㅇㅇ",
      "작성일" : "08.15",
      "조회" : 792,
      "추천" : 10,
    },
    {
      "번호" : 22288,
      "말머리" : "일반",
      "제목" : "정처 기능사 시험 코딩중에 [2]",
      "글쓴이" : "ㅇㅇ",
      "작성일" : "08.15",
      "조회" : 151,
      "추천" : 0,
    },
    {
      "번호" : 22287,
      "말머리" : "일반",
      "제목" : "정보처리기능사 어땠어 [5]",
      "글쓴이" : "Reina",
      "작성일" : "08.15",
      "조회" : 279,
      "추천" : 10,
    },
    {
      "번호" : 22286,
      "말머리" : "일반",
      "제목" : "비전공자 정처기 도전에 고통받는점 [4]",
      "글쓴이" : "ㅇㅇ(39.116)",
      "작성일" : "08.15",
      "조회" : 198,
      "추천" : 0,
    },
    {
      "번호" : 22285,
      "말머리" : "일반",
      "제목" : "9급 전산직 막 준비하기 시작하는 비전공자인데 [6]",
      "글쓴이" : "ㅇㅇ(223.38)",
      "작성일" : "08.15",
      "조회" : 112,
      "추천" : 0,
    },
    {
      "번호" : 22284,
      "말머리" : "일반",
      "제목" : "아오 뷰 틀린게 제일 빡치네",
      "글쓴이" : "ㅇ(121.149)",
      "작성일" : "08.15",
      "조회" : 161,
      "추천" : 0,
    },
    {
      "번호" : 22283,
      "말머리" : "일반",
      "제목" : "혹시 산업기사실기 어떤 방식으로 치나요? [1]",
      "글쓴이" : "ㅇㅇ(61.79)",
      "작성일" : "08.15",
      "조회" : 140,
      "추천" : 0,
    },
    {
      "번호" : 22282,
      "말머리" : "일반",
      "제목" : "실기 교재 어디꺼가 갑이야? [4]",
      "글쓴이" : "호루스갤로그",
      "작성일" : "08.15",
      "조회" : 236,
      "추천" : 0,
    },
    {
      "번호" : 22281,
      "말머리" : "일반",
      "제목" : "55점 ㅈ같다 [1]",
      "글쓴이" : "ㅇㅇ",
      "작성일" : "08.15",
      "조회" : 131,
      "추천" : 0,
    },
    {
      "번호" : 22280,
      "말머리" : "일반",
      "제목" : "비트로커 [6]",
      "글쓴이" : "ㅇㅇ(106.101)",
      "작성일" : "08.15",
      "조회" : 107,
      "추천" : 0,
    },
    {
      "번호" : 22279,
      "말머리" : "일반",
      "제목" : "떨어졌다 [1]",
      "글쓴이" : "ㅇㅇ(183.102)",
      "작성일" : "08.15",
      "조회" : 140,
      "추천" : 0,
    },
    {
      "번호" : 22278,
      "말머리" : "일반",
      "제목" : "아 55점이네 [1]",
      "글쓴이" : "ㅇㅇ(180.230)",
      "작성일" : "08.15",
      "조회" : 139,
      "추천" : 0,
    },
    {
      "번호" : 22277,
      "말머리" : "일반",
      "제목" : "tcp문제에 혹시 '연결형'이란 단어도 있었음? [12]",
      "글쓴이" : "ㅇㅇ(223.62)",
      "작성일" : "08.15",
      "조회" : 47,
      "추천" : 0,
    },
    {
      "번호" : 22276,
      "말머리" : "일반",
      "제목" : "916문제가 [2]",
      "글쓴이" : "ㅇㅇ(122.45)",
      "작성일" : "08.15",
      "조회" : 106,
      "추천" : 0,
    },
  };

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(child: Container(
          color: Colors.indigo,
        )),
        Container(
          color: Colors.amber,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.7 > 700 ? MediaQuery.of(context).size.width * 0.7 : 700,
                  child: Column(
                    children: [
                      Container(
                        height: 100,
                        child: Text(
                            "게시판",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 48,
                            ),
                          ),
                      ),
                      Row(
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "게시판",
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: Colors.blue
                              ),
                            ),
                          ),
                          Spacer(),
                          Container(
                            width: 150,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.black
                              ),
                              onPressed: (){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => WriteScreen()),
                                );
                              },
                              child: Text(
                                "글쓰기",
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 20,
                                    color: Colors.yellow
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Divider(),
                      Row(
                        children: [
                          Text("추천순"),
                          Spacer(
                            flex: 1,
                          ),
                          Text("개념글"),
                          Spacer(
                            flex: 5,
                          ),
                          Spacer(),
                        ],
                      ),
                      Divider(),
                      Row(
                        children: [
                          Container(
                            width: 10,
                          ),
                          Container(
                            width: 200,
                              child: Text("번호")
                          ),
                          Container(
                            width: 180,
                              child: Text("제목")
                          ),
                          Container(
                            width: 70,
                              child: Text("글쓴이")
                          ),
                          Container(
                            width: 70,
                              child: Text("글쓴날")
                          ),
                          Container(
                              width: 70,
                              child: Text("조회")
                          ),
                          Container(
                              width: 70,
                              child: Text("추천")
                          ),
                        ],

                      ),
                      SizedBox(
                        height: 500,
                        child:
                          ListView.builder(
                            itemCount: data.length,
                              padding: EdgeInsets.all(8),
                              itemBuilder: (BuildContext context, int index){
                                return Container(
                                  width: 300,
                                  height: 50,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 10,
                                      color: Colors.orange,
                                    ),
                                  ),
                                  child: Row(
                                    children: [

                                      Container(
                                        width: 70,
                                        child: Text(
                                          '${data.toList()[index]['번호']}',
                                          style: TextStyle(fontSize: 13),
                                          maxLines: 1,
                                          softWrap: false,
                                          overflow: TextOverflow.fade,
                                        ),
                                      ),
                                      Container(
                                        width: 300,
                                        child: Text(
                                          '${data.toList()[index]['제목']}',
                                          style: TextStyle(fontSize: 20),
                                          maxLines: 1,
                                          softWrap: false,
                                          overflow: TextOverflow.fade,
                                        ),
                                      ),
                                      Container(
                                        width: 70,
                                        child: Text('${data.toList()[index]['글쓴이']}')
                                      ),
                                      Container(
                                        width: 70,
                                          child: Text('${data.toList()[index]['작성일']}')
                                      ),
                                      Container(
                                         width: 70,
                                          child: Text('${data.toList()[index]['조회']}')),
                                      Container(
                                        width: 70,
                                          child: Text('${data.toList()[index]['추천']}')
                                      ),
                                    ],
                                  ),
                                );
                              })
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(child: Container(
          color: Colors.lightGreen,
        )),
      ],
    );

  }
}